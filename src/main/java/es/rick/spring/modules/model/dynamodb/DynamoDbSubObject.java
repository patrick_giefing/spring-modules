package es.rick.spring.modules.model.dynamodb;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBDocument
public class DynamoDbSubObject {
    private String a;
    private String b;
    private List<DynamoDbSubObject> subList;
}
