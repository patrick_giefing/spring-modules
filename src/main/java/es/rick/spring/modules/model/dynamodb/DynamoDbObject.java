package es.rick.spring.modules.model.dynamodb;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "object")
public class DynamoDbObject {
    @DynamoDBHashKey
    private String key;
    private DynamoDbSubObject sub;
    private List<DynamoDbSubObject> subList;
}
