package es.rick.spring.modules.controller;

import es.rick.spring.modules.model.dynamodb.DynamoDbObject;
import es.rick.spring.modules.model.dynamodb.DynamoDbSubObject;
import es.rick.spring.modules.repo.dynamodb.DynamoDbObjectRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("dynamo")
public class DynamoDbController {
    private final DynamoDbObjectRepo dynamoDbObjectRepo;

    @Autowired
    public DynamoDbController(DynamoDbObjectRepo dynamoDbObjectRepo) {
        this.dynamoDbObjectRepo = dynamoDbObjectRepo;
    }

    @GetMapping
    public Iterable<DynamoDbObject> getObjects() {
        Iterable<DynamoDbObject> objects = dynamoDbObjectRepo.findAll();
        return objects;
    }

    @DeleteMapping("{key}")
    public void deleteObject(@PathVariable("key") String key) {
        dynamoDbObjectRepo.deleteById(key);
    }

    @PostMapping
    public void insertDummyObjects() {
        var subObject1 = new DynamoDbSubObject("a1", "b1", null);
        var subObject2 = new DynamoDbSubObject("a2", "b2", List.of(subObject1));
        var subObject3 = new DynamoDbSubObject("a3", "b3", null);
        var subObject4 = new DynamoDbSubObject("a4", "b4", null);
        var subObject5 = new DynamoDbSubObject("a5", "b5", List.of(subObject3, subObject4));
        DynamoDbObject object = new DynamoDbObject("v1", subObject1, List.of(subObject2, subObject5));
        dynamoDbObjectRepo.save(object);
    }
}
