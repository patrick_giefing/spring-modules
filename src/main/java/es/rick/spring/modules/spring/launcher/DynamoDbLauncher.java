package es.rick.spring.modules.spring.launcher;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import es.rick.spring.modules.ProjectRoot;
import es.rick.spring.modules.repo.dynamodb.DynamoDbObjectRepo;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;

@SpringBootApplication(scanBasePackageClasses = ProjectRoot.class)
@EnableDynamoDBRepositories(basePackageClasses = DynamoDbObjectRepo.class)
public class DynamoDbLauncher {
    public static void main(String[] args) {
        SpringApplication.run(DynamoDbLauncher.class, args);
    }

    @Bean
    public AmazonDynamoDB amazonDynamoDB(
            @Value("${amazon.dynamodb.endpoint}") String amazonDynamoDBEndpoint,
            @Value("${amazon.aws.accesskey}") String amazonAWSAccessKey,
            @Value("${amazon.aws.secretkey}") String amazonAWSSecretKey
    ) {
        BasicAWSCredentials amazonAWSCredentials = new BasicAWSCredentials(
                amazonAWSAccessKey, amazonAWSSecretKey);
        AmazonDynamoDB amazonDynamoDB = new AmazonDynamoDBClient(amazonAWSCredentials);
        if (!StringUtils.isEmpty(amazonDynamoDBEndpoint)) {
            amazonDynamoDB.setEndpoint(amazonDynamoDBEndpoint);
        }
        return amazonDynamoDB;
    }
}
