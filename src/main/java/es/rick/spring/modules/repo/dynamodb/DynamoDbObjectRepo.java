package es.rick.spring.modules.repo.dynamodb;

import es.rick.spring.modules.model.dynamodb.DynamoDbObject;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.PagingAndSortingRepository;

@EnableScan
public interface DynamoDbObjectRepo extends PagingAndSortingRepository<DynamoDbObject, String> {
}
